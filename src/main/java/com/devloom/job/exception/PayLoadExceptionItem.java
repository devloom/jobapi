package com.devloom.job.exception;

public enum PayLoadExceptionItem {
    SYSTEM_ERROR("API SYSTEM ERROR", 99),
    ERROR("", 100),
    INVALID_INPUT("Invalid INPUT", 101),
    HTTP_CALL_ERROR("Http call error", 102),
    RESOURCE_EXCEPTION("", 103),
    NOT_FOUND_USER_SECURITY_CONTEXT("NOT_FOUND_USER_SECURITY_CONTEXT", 104),
    INCORRECT_CREDENTIALS("Login/Password not match !", 105),
    CUSTOMER_NOT_FOUND("CUSTOMER_NOT_FOUND", 106),
    UNAUTORIZED("UNAUTORIZED", 107),
    JOB_NOT_FOUND("JOB_NOT_FOUND", 108),
    ;

    private String message;
    private String detail;
    private Integer statusCode;

    PayLoadExceptionItem(String message, Integer statusCode) {
        this.message = message;
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public PayLoadExceptionItem detail(String d) {
        this.detail = d;
        return this;
    }

    public PayLoadExceptionItem message(String m) {
        this.message = m;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    @Override
    public String toString() {
        return (
                "PayLoadExceptionItem{" +
                        "message='" +
                        message +
                        '\'' +
                        ", detail='" +
                        detail +
                        '\'' +
                        ", statusCode=" +
                        statusCode +
                        '}'
        );
    }
}
