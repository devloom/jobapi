package com.devloom.job.services.impl;

import com.devloom.job.entity.Job;
import com.devloom.job.exception.ExceptionApi;
import com.devloom.job.exception.PayLoadExceptionItem;
import com.devloom.job.pojo.dto.JobDto;
import com.devloom.job.pojo.mapper.JobMapper;
import com.devloom.job.pojo.req.JobReq;
import com.devloom.job.repo.JobRepo;
import com.devloom.job.services.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;


@Component
public class JobServiceImpl implements JobService {

    @Autowired
    private JobRepo repo;

    @Autowired
    private JobMapper mapper;

    @Override
    public void createJob(JobReq req) {

    }

    @Override
    public void updateJob(JobReq req, String jobId) {

    }

    @Override
    public void deleteJob(String jobId) {

    }

    @Override
    public JobDto jobDetails(String jobId) {
        return mapper.toJobDto(byId(jobId));
    }

    @Override
    public Page<JobDto> jobs(int page, int size) {
        return null;
    }

    @Override
    public Job byId(String jobId) {
        return repo.findById(jobId).orElseThrow(()->{
            throw new ExceptionApi(PayLoadExceptionItem.JOB_NOT_FOUND);
        });
    }
}
