package com.devloom.job.services;

import com.devloom.job.entity.Job;
import com.devloom.job.pojo.dto.JobDto;
import com.devloom.job.pojo.req.JobReq;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public interface JobService {

   void createJob(JobReq req);
   void updateJob(JobReq req,String jobId);
   void deleteJob(String jobId);

   JobDto jobDetails(String jobId);
   Page<JobDto> jobs(int page,int size);



   Job byId(String jobId);
}
