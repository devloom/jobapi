package com.devloom.job.controllers;


import com.devloom.job.pojo.dto.JobDto;
import com.devloom.job.pojo.req.JobReq;
import com.devloom.job.services.JobService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/job")
@CrossOrigin
public class JobController {

    @Autowired
    private JobService jobService;

    @Operation(summary = "Get job details by id")
    @GetMapping("/")
    public ResponseEntity<JobDto> jobById(@RequestParam String jobId) {
        return new ResponseEntity<>(jobService.jobDetails(jobId), HttpStatus.OK);
    }

    @Operation(summary = "Get All jobs ")
    @GetMapping("/all")
    public ResponseEntity<Page<JobDto>> all(@RequestParam int page, @RequestParam int size) {
        return new ResponseEntity<>(jobService.jobs(page,size), HttpStatus.OK);
    }

    @Operation(summary = "create new Job ")
    @PostMapping("/")
    public void create(@RequestBody JobReq req) {
        jobService.createJob(req);
    }

    @Operation(summary = "update Job ")
    @PatchMapping("/")
    public void update(@RequestBody JobReq req,@RequestParam String jobId) {
        jobService.updateJob(req,jobId);
    }

    @Operation(summary = "delete Job ")
    @DeleteMapping("/")
    public void delete(@RequestParam String jobId) {
        jobService.deleteJob(jobId);
    }

}
