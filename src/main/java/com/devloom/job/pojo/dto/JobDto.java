package com.devloom.job.pojo.dto;

import lombok.Data;

@Data
public class JobDto {

    private String title;

    //request field to return from job
}
