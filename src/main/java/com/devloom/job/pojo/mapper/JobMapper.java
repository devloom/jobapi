package com.devloom.job.pojo.mapper;

import com.devloom.job.entity.Job;
import com.devloom.job.pojo.dto.JobDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper(componentModel = "spring")
@Service
public interface JobMapper {

    JobDto toJobDto(Job j);
    List<JobDto> toJobDto(List<Job> j);

}
