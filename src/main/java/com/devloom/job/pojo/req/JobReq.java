package com.devloom.job.pojo.req;

import lombok.Data;

@Data
public class JobReq {

    private String title;

    //request field to create job
}
