package com.devloom.job.pojo.enumeration;

public enum JobStatus {
    ENABLED,
    DISABLED
}
